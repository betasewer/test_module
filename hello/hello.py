
class Hello:
    """ @type
    パッケージテスト用の型。
    2022/01/07 
    """
    def hello(self):
        """ @method
        あいさつをする。
        Returns:
            Int: 1
        """
        print("Hello?")
        return 1
        
    def factorial(self, n):
        """ @method
        Nの階乗を求める。
        Params:
            n(Int): N
        Returns:
            Int: 階乗
        """
        s = 1
        for x in range(2, n+1):
            s = s * x
        return s
    