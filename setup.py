from setuptools import setup, find_packages

setup(
    name="test_module",
    version="0.0.0.1",
    
    packages=["hello"],
    
    author='Goro Sakata',
    author_email='gorosakata@ya.ru',
    
    description='test module for machaon package system',
)
